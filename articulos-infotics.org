* PUBLICADO Bloques de Orgmode en Orgmode :orgmode:org:emacs:bloques:tangle:
:PROPERTIES:
:EXPORT_DATE: <2018-07-15 dom>
:EXPORT_FILE_NAME: bloques-orgmode-en-orgmode
:END:
Tal como dice la [[https://emacs.stackexchange.com/questions/19656/tangling-fundamental-source-blocks-with-org-header-like-code][receta]] de [[https://emacs.stackexchange.com/users/2370/tobias][Tobias]], es importante que consideremos la
edición del código en buffer nuevo con la opción =C-c '= ya que así
maneja -escapa- correctamente la sintaxis del bloque de Orgmode.

1. Empezamos un bloque de código como otro cualquiera, es decir,
   escribiendo =<s= y pulsando la tabulación para que autocomplete.
2. Luego, escribimos el tipo de código, que en este caso le vamos a
   llamar =fundamental=, con la opción =:tangle= y a continuación el
   nombre del archivo donde quieres que vaya todo el texto de ese
   bloque y de los de su misma sesión, si los hubiera (atributo
   =:session=).
3. Escribe =C-c '= para editar el bloque de código en un buffer aparte.
4. Una vez que hayas terminado, se completará el texto en el bloque de
   código con el parseo correcto para que no lo interprete igual, es
   decir, con una =,= delante de cada línea que tiene sintaxis Orgmode.

Dado que Orgmode no se ejecuta, lo único que queremos es que se agrupe
todo lo que sea susceptible de juntarse, es decir, allá donde se haya
usado el atributo =tangle= . Para producir el archivo, ~tangleamos~ , es
decir, =C-c C-v t=:

#+BEGIN_SRC orgmode :tangle test.inp
  ,* Soy el primer encabezamiento de primer nivel de test.inp
  Ahora viene un texto

  Y ahora, un listado:
  - Un elemento
  - Otro elemento

  ,* Praesent fermentum tempor tellus.

  Phasellus neque orci, porta a, aliquet quis, semper a, massa.

  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit tempor tellus.  Donec pretium posuere tellus.  Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus.  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  Nulla posuere.  Donec vitae dolor.  Nullam tristique diam non turpis.  Cras placerat accumsan nulla.  Nullam rutrum.  Nam vestibulum accumsan nisl.

  ,** Bibliografía
  - La red
#+END_SRC

* PUBLICADO Orgmode y los bloques, también de código :orgmode:bloques:src:babel:template:
:PROPERTIES:
:EXPORT_FILE_NAME: bloques-en-orgmode
:EXPORT_DATE: [2018-07-16 lun]
:END:
Tal como explica [[http://www.johndcook.com/blog/2012/02/09/python-org-mode/][John D. Cook]] en un artículo sobre *programación
literaria* (lo vemos en otro momento), /Orgmode/ permite manejar
distintos y variados bloques de texto en un documento de texto, y
estos bloques pueden ser incluso bloques de código.

Con cualquiera de estos bloques se pueden hacer cosas, como por
ejemplo, si se tratan de código, ejecutar volcar los resultados al
documento de texto, ya sea luego exportado a HTML o PDF, y mostrar a
la vez el código y el resultado de ejecutar el código.

Nombrando los bloques de código, podemos referenciarlos para
operaciones posteriores.

(Para una lectura sobre programación lteraria e investigación reproducible, recomiendo el artículo [[http://www.jstatsoft.org/v46/i03/paper][A Multi-Language Computing Environment for Literate Programming and Reproducible Research]].)

** Insertar bloques de código

 Para insertar un elemento estructural, se escribe el carácter =<= seguido de un selector de plantillas de bloques de código que activamos con el tabulador /<TAB>/. Estas son las opciones que aparecen:

 #+BEGIN_SRC org
   s 	#+BEGIN_SRC ... #+END_SRC
   e 	#+BEGIN_EXAMPLE ... #+END_EXAMPLE
   q 	#+BEGIN_QUOTE ... #+END_QUOTE
   v 	#+BEGIN_VERSE ... #+END_VERSE
   c 	#+BEGIN_CENTER ... #+END_CENTER
   l 	#+BEGIN_LaTeX ... #+END_LaTeX
   L 	#+LaTeX:
   h 	#+BEGIN_HTML ... #+END_HTML
   H 	#+HTML:
   a 	#+BEGIN_ASCII ... #+END_ASCII
   A 	#+ASCII:
   i 	#+INDEX: line
   I 	#+INCLUDE: line 

 #+END_SRC

 Por ejemplo, si se escribe =<h= se expande un bloque de código /HTML/:
 #+BEGIN_SRC org
   ,#+BEGIN_HTML
   <p>Hola, Mundo!!!</p>
   ,#+END_HTML
 #+END_SRC

En cambio, si hubiéramos tecleado =<H= , habríamos encontrado una línea
donde escribir HTML dentro del documento Orgmode:

#+BEGIN_SRC org

  ,#+HTML: <div id="ejemplo"><p>Hola, Mundo</p></div>


#+END_SRC

O bien se puede ejecutar =bash=:

 #+name: directorios
 #+BEGIN_SRC shell :results output
 echo "hola, mundo"

 #+END_SRC

 #+RESULTS: directorios
 : hola, mundo

Perl:

#+name: perl
#+begin_src perl
  sub my_func {
          print "Hello, world!\n";
  }
  print my_func;
#+end_src

#+RESULTS: perl
: 1

O cualquier otro lenguaje, mientras lo tengamos configurado correctamente con Babel.

** Añadir lenguajes

Este es un ejemplo de configuración de Babel en el archivo que corresponda:
 #+BEGIN_SRC emacs-lisp
 (org-babel-do-load-languages
  'org-babel-load-languages
   '( (perl . t)         
      (ruby . t)
      (sh . t)
      (python . t)
      (emacs-lisp . t) 
      (sqlite . t)
      (R . t)
    ))

 #+END_SRC

Donde hemos configurado perl, ruby, bash, python, emacs-lisp, sqlite y R.

Para añadir lenguajes, hay que instalar su correspondiente conector.

** Añadir templates

Una vez que te acostumbras a insertar los bloques de código, encuentras de utilidad personalizarlos o añadir otros.

Con la variable =org-structure-template-alist= se pueden añadir atajos para la inserción de bloques de código. Por ejemplo, así se añaden bloques de código /emacs-lisp/ con el atajo =<el=

 #+BEGIN_SRC emacs-lisp
 (add-to-list 'org-structure-template-alist
              '("el" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC" "<src lang=\"emacs-lisp\">\n?\n</src>"))

 #+END_SRC

* PUBLICADO Recuperar grub                     :grub:recuperar:windows:linux:
:PROPERTIES:
:EXPORT_DATE: <2017-02-13 lun>
:EXPORT_FILE_NAME: recuperar-grub
:END:
Recientemente tuve que recuperar Grub2 de uno de los equipos que
manejo. Es un ordenador con arranque dual Windows 7 y Debian que tenía
tan solo 60 GB para Windows y en cuanto quieres hacer algo se queda
corto.

La solución fue entrar con un USB como si fuéramos a reinstalar Debian
y redimensionar la partición con Debian. Todo fue rápido y en
apariencia sin problemas, pero al reiniciar salió el menú de grub, el
gestor de arranque:

#+BEGIN_EXAMPLE
grub rescue>
#+END_EXAMPLE

Esto significa que grub ha perdido la partición desde la que debe
arrancar, porque da error o porque no exista.

Una opción para recuperarlo sería ir con una distro Linux desde la que
arrancar on un sistema operativo completo, una LiveCD. Es fácil, pero
tienes que tener una distro a mano en un USB.

Sin embargo, por la consola de rescate de grub también podemos
hacerlo.

Primero, tenemos que descubrir en qué partición está. Para
ello, tenemos que ejecutar ls y ver cuántas particiones hay:

#+BEGIN_EXAMPLE
grub rescue> ls
#+END_EXAMPLE

Lo que saldrá algo parecido a esto:
#+BEGIN_EXAMPLE
(hd0) (hd0,1) (hd1) (hd1,1) (hd1,5) (hd2) (hd2,1) (hd3) (hd3,1)
#+END_EXAMPLE

Cada =hd= implica un /hard disk/, un disco duro, y a partir de ahí se
crean particiones =/dev/sd=.

Siguiendo el caso anterior:
- =hd0,1= es /dev/sda1
- =hd1,1= es /dev/sdb1
- =hd2,1= es /dev/sdc1
- etc...

Hay que probar a listar cada partición a ver cuál tiene boot:

#+BEGIN_EXAMPLE
ls (hd1,1)/
#+END_EXAMPLE

Una vez que lo hemos identificado, añadimos el prefijo:

#+BEGIN_EXAMPLE
set prefix=(hd1,1)/boot/grub
#+END_EXAMPLE

Ahora ampliamos los comandos disponibles con =insmod=:

#+BEGIN_EXAMPLE
insmod (hd1,1)/boot/grub/linux.mod
#+END_EXAMPLE

Aunque a mí este ejemplo me dio error y me funcionó simplemente
=insmod linux=

Ahora seleccionamos la partición:
#+BEGIN_EXAMPLE
set root=(hd1,1)
#+END_EXAMPLE

Y cargamos la imagen que queramos:

#+BEGIN_EXAMPLE
iunux /boot/vmlinuz-3.2.0-38-generic root=/dev/sdb1

#+END_EXAMPLE

¡Y listo!

* PUBLICADO Instalar varios paquetes en R a la vez              :R:funciones:
:PROPERTIES:
:EXPORT_DATE: <2018-07-18 mié>
:EXPORT_FILE_NAME: instalar-varios-paquetes-R-a-la-vez
:END:

Más de una vez tendrás que instalar varios paquetes de R antes de
realizar algún ejercicio. Resulta bastante tedioso escribir
continuamente ~install.packages("nombre-paquete")~.

Pues bien, hay al menos dos opciones: la función =ipak= o el paquete
=easypackages=

** La función ipak

[[https://gist.github.com/stevenworthington/3178163][Steven Worthington]] ha creado una función, para instalar varios paquetes a la vez, =ipak=, que comprueba que los paquets están instalados, los instala si no lo están y los carga en la sesión.

#+BEGIN_EXAMPLE
# Definimos ipak
ipak <- function(pkg){
    new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
    if (length(new.pkg)) 
        install.packages(new.pkg, dependencies = TRUE)
    sapply(pkg, require, character.only = TRUE)
}

# Creamos una variable con el listado de paquetes que llamaremos packages

packages <- c("ggplot2", "plyr", "reshape2", "RColorBrewer", "scales", "grid")

# invocamos con la función ipak la variable packages

ipak(packages)
#+END_EXAMPLE

Si seguís el /gist/, alguna persona ha dejado su lista ideal:

#+BEGIN_EXAMPLE
packages <- c("ggplot2", "plyr", "reshape2", "RColorBrewer", "scales", "grid", c("plyr","digest","ggplot2","colorspace","stringr","RColorBrewer","reshape2","zoo","proto","scales","car","dichromat","gtable","munsell","labeling","Hmisc","rJava","mvtnorm","bitops","rgl","foreign","XML","lattice","e1071","gtools","sp","gdata","Rcpp","MASS","Matrix","lmtest","survival","caTools","multcomp","RCurl","knitr","xtable","xts","rpart","evaluate","RODBC","tseries","DBI","nlme","lme4","reshape","sandwich","leaps","gplots","abind","randomForest","Rcmdr","coda","maps","igraph","formatR","maptools","RSQLite","psych","KernSmooth","rgdal","RcppArmadillo","effects","sem","vcd","XLConnect","markdown","timeSeries","timeDate","RJSONIO","cluster","scatterplot3d","nnet","fBasics","forecast","quantreg","foreach","chron","plotrix","matrixcalc","aplpack","strucchange","iterators","mgcv","kernlab","SparseM","tree","robustbase","vegan","devtools","latticeExtra","modeltools","xlsx","slam","TTR","quantmod","relimp","akima","memoise"))
#+END_EXAMPLE

** Paquete easypackages

Otra opción es utilizar el paquete [[https://cran.r-project.org/web/packages/easypackages/README.html][easypackages]] que permite eso mismo:

Si queremos la opción de desarrollo, hemos de tener instalado =devtools=:

#+BEGIN_EXAMPLE
install.packages("devtools")
#+END_EXAMPLE

Y luego:
#+BEGIN_EXAMPLE
devtools::install_github("jakesherman/packages")
#+END_EXAMPLE

Si no, instalamos directamente la librería:

#+BEGIN_EXAMPLE
install.packages("easypackages")
#+END_EXAMPLE

La invocamos:

#+BEGIN_EXAMPLE
library(easypackages)
#+END_EXAMPLE

Y ya podemos instalar varios de una vez:
#+BEGIN_EXAMPLE
libraries("dplyr", "ggplot2", "data.table")
#+END_EXAMPLE

* PUBLICADO Crea tu clave GNUPG        :gnupg:gpg:privacidad:datospersonales:
:PROPERTIES:
:EXPORT_DATE: <2018-09-02>
:EXPORT_FILE_NAME: manual-clave-gnupg-gpg
:END:
Vistos distintos manuales y blogs, recopilo aquí la experiencia con gnupg en Debian GNU/Linux.

** Previo: modifica tu configuración a SHA2
Lo primero, vamos a cambiar la configuración de gpg para que utilice SHA2 en vez de SHA1. Por ello, añadimos estas líneas al final del archivo =.gnupg/gpg.conf=:
 #+BEGIN_EXAMPLE sh
personal-digest-preferences SHA256
cert-digest-algo SHA256
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
 #+END_EXAMPLE
** Manual y ayuda
Recuerda que siempre puedes acceder al manual desde la terminal con =man gpg=, y también puedes ver una ayuda rápida con =gpg --help.=
** Crear una nueva clave GPG

Creamos la clave con la opción =--gen-key= o si queremos que salgan todas las opciones con la opción =--full-generate-key=

#+BEGIN_EXAMPLE
gpg --full-generate-key
#+END_EXAMPLE

Tras un mensaje de gpg, saldrá este diálogo:

#+BEGIN_EXAMPLE
Por favor seleccione tipo de clave deseado:
   (1) RSA y RSA (por defecto)
   (2) DSA y ElGamal
   (3) DSA (sólo firmar)
   (4) RSA (sólo firmar)
Su elección: 

#+END_EXAMPLE

RSA o DSA... elige lo que prefieras o bien la opción por defecto. Si quieres investigar más, puedes empezar por aquí: https://lists.gnupg.org/pipermail/gnupg-users/2004-June/022764.html

Una vez elegida esto, nos pedirá la longitud de la clave:

#+BEGIN_EXAMPLE
las claves RSA pueden tener entre 1024 y 4096 bits de longitud.
¿De qué tamaño quiere la clave? (3072) 

#+END_EXAMPLE

Saldrán varias opciones, recomendamos preferiblemente de 4096 bit

También te pedirá si quieres que expire algún día o que no expire:

#+BEGIN_EXAMPLE
Por favor, especifique el período de validez de la clave.
         0 = la clave nunca caduca
      <n>  = la clave caduca en n días
      <n>w = la clave caduca en n semanas
      <n>m = la clave caduca en n meses
      <n>y = la clave caduca en n años
¿Validez de la clave (0)? 

#+END_EXAMPLE

Entonces pasará a pedir unos datos:

- Nombre y Apellidos
- Dirección de correo electrónico
- Comentario

Estos datos aparecerán tal que así:

#+BEGIN_EXAMPLE
"Nombre y apellidos (comentario) <cuenta@correo.org>"
#+END_EXAMPLE

Si nos equivocamos o queremos cambiarlo, podremos hacerlo luego.

Nos pedirá que confirmemos:

#+BEGIN_EXAMPLE
¿Cambia (N)ombre, (C)omentario, (D)irección o (V)ale/(S)alir? 

#+END_EXAMPLE

Y pasaremos a la contraseña. En el diálogo, nos sale un aviso:

#+BEGIN_EXAMPLE
Es necesario generar muchos bytes aleatorios. Es una buena idea realizar
alguna otra tarea (trabajar en otra ventana/consola, mover el ratón, usar
la red y los discos) durante la generación de números primos. Esto da al
generador de números aleatorios mayor oportunidad de recoger suficiente
entropía.

#+END_EXAMPLE

Una vez repitamos correctamente la contraseña, ya habremos creado la clave GPG.
*** Generar subclave

Parece una buena ida crear una subclave de la clave creada porque así utilizaremos esta en vez de la principal.

Dado que lo hacemos sobre nuestra clave, veamos si tenemos solo la nuestra o más claves, incluso nuestras antiguas, y editamos la que acabamos de crear:
#+BEGIN_EXAMPLE
gpg --list-keys cuenta@dominio.tld
#+END_EXAMPLE

Con el /hash/ (sucesión de letras en mayúsculas y números) que salga después de =pub=
se identifica la clave y se selecciona:

#+BEGIN_EXAMPLE
gpg --edit-key hash
#+END_EXAMPLE

El prompt de la consola cambiara al de gpg, y añadimos clave:
#+BEGIN_EXAMPLE
gpg> addkey
#+END_EXAMPLE

Hay que seleccionar el tipo de clave deseado, entre las siguientes opciones:
#+BEGIN_EXAMPLE
(3) DSA (sólo firmar)
(4) RSA (sólo firmar)
(5) ElGamal (sólo cifrar)
(6) RSA (sólo cifrar)
#+END_EXAMPLE

Seleccionamos la opción 6, ya que solo queremos cifrar con esta subclave.

La siguiente pregunta es qué longitud de bits queremos... cuanto más, mejor:

#+BEGIN_EXAMPLE
las claves RSA pueden tener entre 1024 y 4096 bits de longitud.
¿De qué tamaño quiere la clave? (3072) 

#+END_EXAMPLE

A continuación, nos pregunta la caducidad de la clave:

#+BEGIN_EXAMPLE
El tamaño requerido es de 4096 bits
Por favor, especifique el período de validez de la clave.
         0 = la clave nunca caduca
      <n>  = la clave caduca en n días
      <n>w = la clave caduca en n semanas
      <n>m = la clave caduca en n meses
      <n>y = la clave caduca en n años
#+END_EXAMPLE

Nos pedirá la contraseña que antes hemos introducido y lo creara.

Ahora solo queda guardar con =save=

#+BEGIN_EXAMPLE
save

#+END_EXAMPLE

** Añadir otra identidad
Para añadir otra identidad, tenemos que editar la clave con la opción =--edit-key= y el identificador de la clave:

#+BEGIN_EXAMPLE
gpg --edit-key hash
#+END_EXAMPLE

Así entramos en el prompt de =gpg= para editar esa clave.

Una vez en la consola de =gpg=, añadiremos la identidad con el comando =adduid=

#+BEGIN_EXAMPLE
adduid
#+END_EXAMPLE

Nos pedirá nombre, dirección de correo electrónico, un comentario (opcional) y validarlo o no. Entonces, pide la contraseña y se crea con la misma expiración que la identidad maestra.

Para finalizar, guardamos con =save=:

#+BEGIN_EXAMPLE
save
#+END_EXAMPLE

** Establecer la identidad primaria

Si queremos cambiar la identidad primaria, editamos la clave con su hash y entramos en el prompt de gpg:

#+BEGIN_EXAMPLE
gpg --edit-key hash
#+END_EXAMPLE

Una vez ahí, seleccionamos la clave:

#+BEGIN_EXAMPLE
uid Número
#+END_EXAMPLE

Y la convertimos en primaria con =primary=:

#+BEGIN_EXAMPLE
primary
#+END_EXAMPLE

Por último, guardamos:

#+BEGIN_EXAMPLE
save
#+END_EXAMPLE

** Listar las claves disponibles

Para saber qué claves tienes, tanto propias como ajenas, puedes listarlas con =gpg --list-keys=

También puede añadir patrones a acontinuación, como por ejemplo el nombre el correo electrónico o el dominio:

#+BEGIN_EXAMPLE
gpg --list-keys patrón
#+END_EXAMPLE

** Exportar clave pública

Para enviar una clave púbica a la otra persona antes hay que exportarla con la opción =--export= y creando un archivo con la salida con la opción =--output=, más el identificador de la clave:
#+BEGIN_EXAMPLE
gpg --output clave-publica-exportada.gpg --export CLAVE_ID
#+END_EXAMPLE

De esta forma crearíamos la clave en formato binario, lo cual puede dar problemas cuando la enviamos por correo electrónico o la mostramos en una página web, por lo que añadiremos la opción =--armor= que fuerza que la salida sea en formato /armadura/ /ASCII/:

#+BEGIN_EXAMPLE
gpg --armor --output clave-publica-exportada.gpg --export CLAVE_ID
#+END_EXAMPLE

** Enviar la llave al servidor
Para enviar una clave al servidor, especificamos el servidor con la opción =keyserver= y la clave con la opción =send-key=:

#+BEGIN_EXAMPLE
gpg --keyserver servidor --send-key ID-de-tu-clave
#+END_EXAMPLE

Puedes enviarlo a uno, dos o varios servidores:
- GNUPG, hkp://keys.gnupg.net
- RedIris, hkp://pgp.rediris.es
- MIT, hkp://pgp.mit.edu
- PGP, hkp://subkeys.pgp.net

** Importar clave pública gpg
Se pueden añadir claves públicas al anillo de claves públicas mediante la opción =--import=

*** De archivo local
#+BEGIN_EXAMPLE
gpg --import archivo-clave-gpg.asc
#+END_EXAMPLE

Lo podemos comprobar con la opción =--list-keys= o con la opción más concreta =--list-public-keys=

*** De servidor de claves
También lo podemos importar de un servidor de claves con la opción =--keyserver=, donde incluimos el servidor en cuestión, y =--search-keys=, donde ponemos el parámetro de búsqueda, algún dato que identifique esa clave, como puede ser el nombre, el correo electrónico o más concretamente su identificador de clave ID_CLAVE:

#+BEGIN_EXAMPLE
gpg --keyserver pgp.mit.edu --search-keys parámetro-de-búsqueda
#+END_EXAMPLE

*** De URL con wget
#+BEGIN_SRC s
wget -O - url-de-clave.asc | gpg --import

#+END_SRC

De esta forma descargamos la clave y lo pasamos a =STDOUT=, para luego importarlo con =gpg --import=

** Validez y firma de claves públicas

GnuPG utiliza un modelo de confianza que no requiere que quien la posee dé validez personalmente a cada clave que importe, pero algunas claves pueden necesitar esta validación personal.

La verificación se realiza comprobando la huella digital de la clave y firmando dicha clave, certificando su validez.

La huella digital se puede ver con la opción =--fingerprint=

#+BEGIN_EXAMPLE
gpg --fingerprint

#+END_EXAMPLE

Para certificarla hemos de editarla con la opción =--edit-key=

#+BEGIN_EXAMPLE
gpg --edit-key CLAVE_ID
#+END_EXAMPLE

Ahí entraremos en una consola de GnuPG desde donde podemos lanzar =fpr= para ver el /fingerprint/ o huella dactilar de la clave.

#+BEGIN_EXAMPLE
gpg> fpr
#+END_EXAMPLE

La huella digital se verifica con quien posee la clave, en persona o por un medio feacientemente seguro.

Si la huella que ofrece es la misma que la clave, podemos estar seguro de la misma y se puede firmar para validarla con el comando =sign=

#+BEGIN_EXAMPLE
gpg> sign
#+END_EXAMPLE

Una vez firmada, podemos comprobar el listado de firmas que lleva la clave con el comando =check=

#+BEGIN_EXAMPLE
gpg> check
#+END_EXAMPLE

** Revocación de claves

Para revocar la clave, hay que tener un certificado de revocación de claves por si se nos olvida la contraseña. De esta manera, publicaremos rápidamente que está revocada.

El certificado de revocación se crea con la opción =--gen-revoke= seguido del identificador de la clave.

A no ser que queramos copiar y pegar el texto de la terminal, podemos utilizar la opción =--output= para crear dar un nombre y crear el archivo con el certificado:

#+BEGIN_EXAMPLE
gpg --output revocacion-ID_CLAVE.cert --gen-revoke 0xIDCLAVE

#+END_EXAMPLE

En los motivos por los que solicitas el certificado puedes dar alguno o simplemente no especificar razón alguna.

Nos pedirá la contraseña.

Si queremos efectivamente revocar esa clave, usaremos el certificado importándolo a nuestro gpg con la opción =--import= y enviando la clave de nuevo al servidor público donde lo hayamos subido:

#+BEGIN_EXAMPLE
gpg --import revocacion-IC_CLAVE.crt
gpg --keyserver servidor --send-keys ID_CLAVE
#+END_EXAMPLE

Este certificado, al igual que todos sus datos, es importante mantenerlo a buen seguro. En este caso más porque podría comprometer su clave. Reproduzco el mensaje de aviso de gpg al crear el certificado:

#+BEGIN_CENTER
Por favor consérvelo en un medio que pueda esconder; si alguien consigue
acceso a este certificado puede usarlo para inutilizar su clave.
Es inteligente imprimir este certificado y guardarlo en otro lugar, por
si acaso su medio resulta imposible de leer. Pero precaución: ¡el sistema
de impresión de su máquina podría almacenar los datos y hacerlos accesibles
a otras personas!

#+END_CENTER

*** Servidores de claves

Algunos de los servidores de claves más utilizados:

- GNUPG, hkp://keys.gnupg.net
- RedIris, hkp://pgp.rediris.es
- MIT, hkp://pgp.mit.edu
- PGP, hkp://subkeys.pgp.net

Aquí podremos enviar la clave, consultarlas por línea de comandos y también por su web, vía https.

** Encriptar y desencriptar datos o cifrar y descifrar documentos

Es importante que entendamos cómo funciona el sistema de clave pública y privada, para ello utilizaremos la metáfora de la *caja fuerte*.

La clave pública es como una caja fuerte. Cuando la persona remitente cifra un documento usando una clave pública, ese documento se pone en la caja fuerte, la caja se cierra y se bloquea. Solo la persona que posee la clave privada de esa clave pública puede recuperar el documento cifrado con esa clave pública.

Cuidado con esto: si quieres enviar un texto a una persona, lo encriptas con su clave pública y borras el documento original, pierdes el documento ya que solo la otra persona será capaz de desencriptar tu documento encriptado.

Por tanto, si quieres mantener también una copia encriptada, encríptalo contra tu clave pública también.


*** Cifrar

Para cifrar un documento usamos la opción =--encrypt=, le damos una salida con la opción =--output= y elegimos la clave con la opción =--recipient=

Ojo, =--recipient= se usa una vez para cada clave destinataria:

#+BEGIN_EXAMPLE
gpg --output documento-firmado.gpg --encrypt --recipient CLAVE_ID documento-que-quiero-cifrar
#+END_EXAMPLE

*** Descifrar

Para descifrar usamos la opción =--decrypt= y una salida con =--output=

#+BEGIN_EXAMPLE
gpg --output documento-descifrado.pdf --decrypt documento-cifrado.gpg
#+END_EXAMPLE

Después de dar tu contraseña de la clave gpg, podrás descifrar el documento.

*** Cifrar con clave de cifrado simétrico

También es posible cifrar documentos sin la criptografía de clave pública con una clave de cifrado simétrico que cifre el documento con la opción --symmetric

#+BEGIN_EXAMPLE
gpg ---output documento-cifrado.gpg --symmetric documento-sin-cifrar
#+END_EXAMPLE

La clave deriva de la contraseña dada en el momento de cifrar el documento y *NUNCA* debe ser igual que la que protege la clave privada.

También puedes utilizar los atajos de las opciones, como por ejemplo para encriptar un archivo:

#+BEGIN_EXAMPLE
gpg -e -s -a archivo
#+END_EXAMPLE

Donde cada letra se corresponde con un atajo:
- =-e:=: =--encrypt=
- =-s=: =--sign=
- =-a=: =--armor=

** Firmar documentos y verificar firmas

Se firman documentos para certificar su autenticidad o añadir una marca de tiempo. Se utiliza el mismo par público y privado de claves pero en una operación diferente a la de cifrado y descifrado:

1. Se genera una firma con la clave privada de quien firma.
2. La firma se verifica por medio de la clave pública correspondiente.

Con la opción =--sign= se elige qué documento se quiere firmar y con la opción =--output= la salida del documento firmado.

#+BEGIN_EXAMPLE
gpg -output documento-firmado.sig -sign documento-que-se-quiere-firmar
#+END_EXAMPLE

El documento se comprime antes de ser firmado y la salida se produce en formato binario.

Para comprobar la firma, utilizamos la opción =--verify= y =--decrypt= para verificar la firma y recuperar el documento original al mismo tiempo. Con la opción =--output= damos salida a la opción verificación o descifrado:

#+BEGIN_EXAMPLE
gpg --output documento-estado-original --decrypt documento-firmado
gpg --output documento-estado-original --verify documento firmado
#+END_EXAMPLE

** Enlaces

- https://gnupg.org/
- http://ekaia.org/blog/2009/05/10/creating-new-gpgkey/
- http://keyring.debian.org/creating-key.html
- http://www.linuxveda.com/2015/03/21/tutorial-gpg-on-linux-encrypt-data/
- https://fedoraproject.org/wiki/Creating_GPG_Keys
- https://archive.fosdem.org/2018/schedule/event/easy_gnupg/

* PUBLICADO Gestión de contenidos con Org-mode y Hugo :hugo:go:golang:orgmode:emacs:
  :PROPERTIES:
  :EXPORT_FILE_NAME: gestion-contenidos-orgmode-hugo
  :EXPORT_DATE: <2018-09-04 mar 01:30>
  :END:
En enero de 2016 [[http://blog.infotics.es/2016/01/18/wellcome-to-jekyll-infotics.html][expliqué]] cómo había pasado la web de infotics.es de [[https://wordpress.org][Wordpress]] a [[https://jekyllrb.com/][Jekyll]]. Ambos se pueden considerar gestores de contenido pero mientras el primero trabaja con PHP y SQL -es decir, genera los contenidos haciendo llamadas con [[https://php.net][PHP]] a la base de datos SQL- el segundo trabaja con un lenguaje de programación -en el caso de Jekyll, con [[https://www.ruby-lang.org][Ruby]]- que se ejecuta en el servidor o en local y general los contenidos estáticos -de ahí el acrónimo [[https://blog.formkeep.com/not-sure-which-static-site-generator-to-pick/][SSG, Static Site Generator]], generador de contenidos estáticos.

Desde ese momento, infotics.es pasó a estar alojado en un servidor con Debian que servía con Apache los contenidos que había generado con Jekyll en local. Aproveché también para cambiarlo al subdominio blog.infotics.es, y así está ahora.

Jekyll no era el único SSG, había otros -[[https://blog.getpelican.com/][Pelican]] o [[https://getnikola.com/][Nikola]] en [[https://www.python.org/][Python]], por ejemplo- pero el soporte de Github -[[https://help.github.com/articles/about-github-pages-and-jekyll/][la web de Github está creada en Jekyll]] porque Jekyll está creado para Github- y las posibilidades de aprender me parecieron una buena opción.

** El flujo de trabajo

Sin embargo, ese primer artículo que anunciaba el cambio también fue el primero de tan solo tres artículos publicados con este sistema:

1. Los artículos se escriben en [[https://orgmode.org][Org-mode]] en [[https://www.gnu.org/software/emacs][Emacs]].
2. Luego los exporto a [[https://daringfireball.net/projects/markdown/][Markdown]].
3. Los copio a la carpeta posts.
4. Actualizo el repositorio git.
5. Se publican.

Este flujo de publicación me resultó complicado porque no lo automaticé absolutamente y, además, porque no me convencía tener el contenido replicado: escrito en Org-mode y luego exportado a markdown para Jekyll.

Por tanto, solo fueron dos más los artículos publicados: [[http://blog.infotics.es/2016/03/21/dia-datos-abiertos-2016/][día de los datos abiertos España 2016]] y [[http://blog.infotics.es/2016/12/03/de-github-a-jekyll-con-git-por-ssh-en-vps-con-debian-apache-letsencrypt/][de github a jekyll con git por ssh en vps con debian, apache y letsencrypt]]. Este último artículo explicaba en detalle todo el reto tecnoloógico del blog que iba y va más allá del propio contenido y que es una apuesta global por la soberanía tecnológica. Y hay otras derivadas por ahí. Pero sigamos con la gestión del contenido.

Aunque seguí escribiendo, mi escritura ya no iba tanto por el formato blog sino por la documentación de todo lo que había aprendido, iba aprendiendo o quería aprender de las tecnologías que me interesan, y hacerlo además siguiendo el paradigma de la programación literaria y la investigación reproducible que están en la esencia de mi elección por [[https://orgmode.org/worg/org-contrib/babel/how-to-use-Org-Babel-for-R.html][Emacs + Orgmode + Babel]]. Esos textos siguen en local, esperando ser publicados. Faltaba, por tanto, encontrar un flujo de publicación mejor o mejorar el utilizado.

Además de los ya citados Nikola y Pelican también he probado [[https://hexo.io/plugins/index.html][Hexo]] ([[https://nodejs.org/][nodejs]]), [[http://renard.github.com/o-blog][o-blog]] (Org-mode), y otros. Durante un tiempo me parecía que Hexo podía ser lo que estaba buscando porque tenía soporte nativo para Orgmode ([[https://github.com/CodeFalling/hexo-renderer-org][plugin hexo-renderer-org]]) y [[https://coldnew.github.io/hexo-org-example/2018/05/22/use-org-download-to-drag-image-to-emacs/][Yen-Chin]] explicaba cómo hacerlo fácil. Suin embargo, la experiencia no fue todo lo satisfactoria que esperaba.

Ojo, con esto no quiero decir que el resto de tecnologías o métodos sean peores, sino que a mí no me han servido. A veces por desconocimiento, por fallos no resueltos, por no dedicarle el tiempo necesario en la mayoría de los casos... Sigamos.

** Hugo

Así llegamos a [[https://gohugo.io/][Hugo]], el framework escrito en [[https://golang.org/][Golang]], que ha sido el elegido "por culpa de" [[https://github.com/kaushalmodi][kaushalmodi]] y su [[https://github.com/kaushalmodi/ox-hugo][ox-hugo]] y que [[https://ox-hugo.scripter.co/doc/examples/][cada vez está utilizando más gente]].

El motivo es que se ha ajustado a lo que esperaba y me funciona. ox-hugo es una extensión para Emacs Org-mode que exporta el contenido de Org-mode al Markdown compatible con Hugo, en concreto a la versión de Markdown /Blackfriday/, incluidas las cabeceras -soporta TOML y YAML- y lo inserta en la estructura de plantillas de Hugo.

De hecho, ox-hugo se basa en [[https://github.com/kaushalmodi/ox-hugo/blob/master/ox-blackfriday.el][ox-blackfriday.el]] -la extensión para exportar Org-mode a Markdown /Blackfriday/- a la que añade la funcionalidad relativa a Hugo.

ox-hugo propone dos vías de exportación que también son dos formas de almacenar/gestionar el propio contenido y el contenido publicado en concreto:

- Un archivo por artículo.
- Un archivo con todos los artículos.

La primera opción fue la que probé inicialmente, ya que se ajustaba a mi flujo de trabajo habitual. Sin embargo, la idea de un archivo único se me antojó interesante, muy en la línea de gestión de contenidos más largos en el mismo documento pero donde las partes del contenido se pueden gestionar en conjunto o por separado. Así ocurre en general con documentos que obedecen a un dominio determinado o que merecen ser compilados, como podría ser un libro, una revista -de papers o de un congreso- o incluso una web. Así ocurrió con el paso de la web de documentos a la web de datos, de alguna manera. Y así podíamos optimizar también la gestión de los contenidos que manejo: cuando empiezas un blog, gestionar un número bajo de artículos parece una tarea realizable; cuando llevas años escribiendo, es un poco más complicado.

Por tanto, me imagino pasar de tener 200 archivos Org-mode a un único archivo, donde cada artículo tiene sus etiquetas y sus propiedades, al estilo Org-mode.

https://d33wubrfki0l68.cloudfront.net/88267a3c2e96cd8ea701ca0b18736ac2fb1ed319/c3e4f/images/one-post-per-subtree.png

Y así lo podéis ver, con cinco artículos publicados desde que empecé en junio de 2018, más éste y muchos otros que espero publicar en breve.

** Archivo fuente

Una copia de los artículos publicados en formato Org-mode se encuentra en [[https://git.fsfe.org/adolflow/infotics.es][este repositorio]].

